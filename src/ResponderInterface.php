<?php

declare(strict_types=1);

namespace Paneric\Interfaces\Responder;

use Psr\Http\Message\ResponseInterface as Response;

interface ResponderInterface
{
    public function __invoke(Response $response, array $data = [], ...$params): Response;
}
