<?php

declare(strict_types=1);

namespace Paneric\Interfaces\Responder;

use Psr\Http\Message\ResponseInterface as Response;

interface ApiResponderInterface
{
    public function __invoke(Response $response, array $data = [], ...$params): Response;
}
